package com.epam.rd.java.basic.task8.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.epam.rd.java.basic.task8.OlxAds;
import com.epam.rd.java.basic.task8.OlxAds.OlxAd;
import com.epam.rd.java.basic.task8.OlxAds.OlxAd.Article;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private ArrayList<OlxAd> res = new ArrayList();

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public ArrayList<OlxAd> getRes() {
		return res;
	}

	public ArrayList<OlxAd> parse(){
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(new File(xmlFileName));
			NodeList olxAdsElements = document.getDocumentElement().getElementsByTagName("olx_ad");
			for(int i =0; i < olxAdsElements.getLength(); i++) {
				Node ad = olxAdsElements.item(i);
				res.add(getOlxAd(ad));
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public void output() throws TransformerException {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("olx_ads");
			rootElement.setAttribute("xmlns", "http://www.nure.ua");
			rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
			doc.appendChild(rootElement);
			for(OlxAd el: res) {
				Element olx_ad = doc.createElement("olx_ad");
				rootElement.appendChild(olx_ad);
				
				Element name = doc.createElement("name");
				name.setTextContent(el.getName());
				olx_ad.appendChild(name);
				
				Element surname = doc.createElement("surname");
				surname.setTextContent(el.getSurname());
				olx_ad.appendChild(surname);
				
				Element phone = doc.createElement("phone");
				phone.setTextContent(el.getPhone());
				olx_ad.appendChild(phone);
				
				Element price = doc.createElement("price");
				price.setTextContent(el.getPrice().toString());
				olx_ad.appendChild(price);
				
				Element article = doc.createElement("article");
				olx_ad.appendChild(article);
				
				Element article_name = doc.createElement("name");
				article_name.setTextContent(el.getArticle().getName());
				article.appendChild(article_name);
				
				Element condition = doc.createElement("condition");
				condition.setTextContent(el.getArticle().getCondition());
				article.appendChild(condition);
				
				Element category = doc.createElement("category");
				category.setTextContent(el.getArticle().getCategory());
				article.appendChild(category);
				
				 try (FileOutputStream output = new FileOutputStream("output.dom.xml")) {
					 writeXml(doc, output);
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void writeXml(Document doc, FileOutputStream output) throws TransformerException {
		 TransformerFactory transformerFactory = TransformerFactory.newInstance();
	     Transformer transformer = transformerFactory.newTransformer();
	     DOMSource source = new DOMSource(doc);
	     StreamResult result = new StreamResult(output);

	     transformer.transform(source, result);
	}

	private static OlxAd getOlxAd(Node node) {
		OlxAd olxAd = new OlxAd();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            olxAd.setName(getTagValue("name", element));
            olxAd.setSurname(getTagValue("surname", element));
            olxAd.setPhone(getTagValue("phone", element));
            olxAd.setPrice(new BigDecimal(getTagValue("price", element)));
            olxAd.setArticle(getArtical(element));
        }
        return olxAd;
    }

	private static Article getArtical(Element element) {
		NodeList nodeList = element.getElementsByTagName("article");
		Article res = new Article();
		Node node = (Node) nodeList.item(0);
		if (node.getNodeType() == Node.ELEMENT_NODE) {
			Element article = (Element) node;
	        res.setName(getTagValue("name", article));
	        res.setCondition(getTagValue("condition", article));
	        res.setCategory(getTagValue("category", article));
	    }
		return res;
	}

	private static String getTagValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = (Node) nodeList.item(0);
        return node.getNodeValue();
    }

}
