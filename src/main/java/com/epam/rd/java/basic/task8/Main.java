package com.epam.rd.java.basic.task8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.epam.rd.java.basic.task8.OlxAds.OlxAd;
import com.epam.rd.java.basic.task8.controller.*;

public class Main {
	
	public static void sortBySurname(ArrayList<OlxAd> olxAds) {
		Comparator comp = new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				return OlxAd.class.cast(o1).getSurname().compareTo(OlxAd.class.cast(o2).getSurname());
			}
		};
		Collections.sort(olxAds, comp);
	}
	
	public static void sortByPrice(ArrayList<OlxAd> olxAds) {
		Comparator comp = new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				return OlxAd.class.cast(o1).getPrice().compareTo(OlxAd.class.cast(o2).getPrice());
			}
		};
		Collections.sort(olxAds, comp);
	}
	
	public static void sortByCondition(ArrayList<OlxAd> olxAds) {
		Comparator comp = new Comparator() {
			@Override
			public int compare(Object o1, Object o2) {
				if(OlxAd.class.cast(o1).getArticle().getCondition().equals("Новий") && OlxAd.class.cast(o2).getArticle().getCondition().equals("Б/У")) {
					return -1;
				}else if(OlxAd.class.cast(o1).getArticle().getCondition().equals("Б/У") && OlxAd.class.cast(o2).getArticle().getCondition().equals("Новий")) {
					return 1;
				}else{
					return 0;
				}
				
			}
		};
		Collections.sort(olxAds, comp);
	}
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		System.out.println("DOM\n");
		// get container
		DOMController domController = new DOMController(xmlFileName);
		domController.parse();
		for(OlxAd el: domController.getRes()) {
			System.out.println(el);
		}
		System.out.println();
		// sort (case 1)
		sortByPrice(domController.getRes());
		for(OlxAd el: domController.getRes()) {
			System.out.println(el);
		}
		
		// save
		String outputXmlFile = "output.dom.xml";
		
		domController.output();

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		System.out.println("SAX\n");
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.parse();
		for(OlxAd el: saxController.getRes()) {
			System.out.println(el);
		}
		System.out.println();
		// sort  (case 2)
		sortBySurname(saxController.getRes());
		for(OlxAd el: saxController.getRes()) {
			System.out.println(el);
		}
		
		// save
		outputXmlFile = "output.sax.xml";
		
		saxController.output();
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		System.out.println("StAX");
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parse();
		for(OlxAd el: staxController.getRes()) {
			System.out.println(el);
		}
		System.out.println();
		// sort  (case 2)
		sortByCondition(staxController.getRes());
		for(OlxAd el: staxController.getRes()) {
			System.out.println(el);
		}
		
		// save
		outputXmlFile = "output.stax.xml";
		staxController.output();
		// PLACE YOUR CODE HERE
	}

}
