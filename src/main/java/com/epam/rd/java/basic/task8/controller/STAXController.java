package com.epam.rd.java.basic.task8.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.Result;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.epam.rd.java.basic.task8.OlxAds.OlxAd;
import com.epam.rd.java.basic.task8.OlxAds.OlxAd.Article;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private ArrayList<OlxAd> res = new ArrayList();
	
	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	
	public ArrayList<OlxAd> getRes() {
		return res;
	}


	public ArrayList<OlxAd> parse(){
		OlxAd olx_ad = null;
		boolean isArticle = false;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try {
			XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
			while(reader.hasNext()) {
				 XMLEvent xmlEvent = reader.nextEvent();
				 if (xmlEvent.isStartElement()) {
					 StartElement startElement = xmlEvent.asStartElement();
					 if (startElement.getName().getLocalPart().equals("olx_ad")) {
						 olx_ad = new OlxAd();
					 }else if(startElement.getName().getLocalPart().equals("name") && !isArticle) {
						 xmlEvent = reader.nextEvent();
						 olx_ad.setName(xmlEvent.asCharacters().getData());
					 }else if(startElement.getName().getLocalPart().equals("surname")) {
						 xmlEvent = reader.nextEvent();
						 olx_ad.setSurname(xmlEvent.asCharacters().getData());
					 }else if(startElement.getName().getLocalPart().equals("phone")) {
						 xmlEvent = reader.nextEvent();
						 olx_ad.setPhone(xmlEvent.asCharacters().getData());
					 }else if(startElement.getName().getLocalPart().equals("price")) {
						 xmlEvent = reader.nextEvent();
						 olx_ad.setPrice(new BigDecimal(xmlEvent.asCharacters().getData()));
					 }else if(startElement.getName().getLocalPart().equals("article")) {
						isArticle = true;
						olx_ad.setArticle(new Article());
					 }else if(startElement.getName().getLocalPart().equals("name") && isArticle) {
						 xmlEvent = reader.nextEvent();
						 olx_ad.getArticle().setName(xmlEvent.asCharacters().getData());
						 isArticle = false;
					 }else if(startElement.getName().getLocalPart().equals("condition")) {
						 xmlEvent = reader.nextEvent();
						 olx_ad.getArticle().setCondition(xmlEvent.asCharacters().getData());
					 }else if(startElement.getName().getLocalPart().equals("category")) {
						 xmlEvent = reader.nextEvent();
						 olx_ad.getArticle().setCategory(xmlEvent.asCharacters().getData());
					 }
				 }
				 if (xmlEvent.isEndElement()) {
					 EndElement endElement = xmlEvent.asEndElement();
	                 if (endElement.getName().getLocalPart().equals("olx_ad")) {
	                	 res.add(olx_ad);
	                 }
	             }
			}
		} catch (IOException | XMLStreamException e) {
			e.printStackTrace();
		}
		return res;
	}
	
	public void output() {
		XMLOutputFactory xof = XMLOutputFactory.newInstance();
		XMLStreamWriter xsw = null;
		try {
			xsw = xof.createXMLStreamWriter(new FileWriter("output.stax.xml"));
			xsw.writeStartDocument();
		    xsw.writeStartElement("olx_ads");
		    xsw.writeAttribute("xmlns", "http://www.nure.ua");
		    xsw.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		    xsw.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
		    for(OlxAd el: res) {
		    	 xsw.writeStartElement("olx_ad");
		    	 xsw.writeStartElement("name");
		    	 xsw.writeCharacters(el.getName());
		    	 xsw.writeEndElement();
		    	 
		    	 xsw.writeStartElement("surname");
		    	 xsw.writeCharacters(el.getSurname());
		    	 xsw.writeEndElement();
		    	 
		    	 xsw.writeStartElement("phone");
		    	 xsw.writeCharacters(el.getPhone());
		    	 xsw.writeEndElement();
		    	 
		    	 xsw.writeStartElement("price");
		    	 xsw.writeCharacters(el.getPrice().toString());
		    	 xsw.writeEndElement();
		    	 
		    	 xsw.writeStartElement("article");
		    	 
		    	 xsw.writeStartElement("name");
		    	 xsw.writeCharacters("abc");
		    	 xsw.writeEndElement();
		    	 
		    	 xsw.writeStartElement("condition");
		    	 xsw.writeCharacters(el.getArticle().getCondition());
		    	 xsw.writeEndElement();
		    	 
		    	 xsw.writeStartElement("category");
		    	 xsw.writeCharacters(el.getArticle().getCategory());
		    	 xsw.writeEndElement();
		    	 
		    	 xsw.writeEndElement();
		    	 xsw.writeEndElement();
		    }
		    xsw.writeEndElement();
		    xsw.writeEndDocument();
		    xsw.flush();
		} catch (XMLStreamException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}