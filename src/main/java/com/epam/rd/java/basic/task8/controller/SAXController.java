package com.epam.rd.java.basic.task8.controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.TransformerException;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.epam.rd.java.basic.task8.OlxAds.OlxAd;
import com.epam.rd.java.basic.task8.OlxAds.OlxAd.Article;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	public ArrayList<OlxAd> res = new ArrayList();
	
	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public ArrayList<OlxAd> getRes() {
		return res;
	}

	public void parse(){
		SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
			SAXParser parser = factory.newSAXParser();
			XMLHandler handler = new XMLHandler();
			parser.parse(new File(xmlFileName), handler);
			res = handler.res;
        } catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void output() throws TransformerException {
		XMLOutputFactory xof = XMLOutputFactory.newInstance();
		XMLStreamWriter xsw = null;
		try {
		    xsw = xof.createXMLStreamWriter(new FileWriter("output.sax.xml"));
		    xsw.writeStartDocument();
		    xsw.writeStartElement("olx_ads");
		    xsw.writeAttribute("xmlns", "http://www.nure.ua");
		    xsw.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		    xsw.writeAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
		    for(OlxAd el: res) {
		    	 xsw.writeStartElement("olx_ad");
		    	 xsw.writeStartElement("name");
		    	 xsw.writeCharacters(el.getName());
		    	 xsw.writeEndElement();
		    	 
		    	 xsw.writeStartElement("surname");
		    	 xsw.writeCharacters(el.getSurname());
		    	 xsw.writeEndElement();
		    	 
		    	 xsw.writeStartElement("phone");
		    	 xsw.writeCharacters(el.getPhone());
		    	 xsw.writeEndElement();
		    	 
		    	 xsw.writeStartElement("price");
		    	 xsw.writeCharacters(el.getPrice().toString());
		    	 xsw.writeEndElement();
		    	 
		    	 xsw.writeStartElement("article");
		    	 
		    	 xsw.writeStartElement("name");
		    	 xsw.writeCharacters(el.getArticle().getName());
		    	 xsw.writeEndElement();
		    	 
		    	 xsw.writeStartElement("condition");
		    	 xsw.writeCharacters(el.getArticle().getCondition());
		    	 xsw.writeEndElement();
		    	 
		    	 xsw.writeStartElement("category");
		    	 xsw.writeCharacters(el.getArticle().getCategory());
		    	 xsw.writeEndElement();
		    	 
		    	 xsw.writeEndElement();
		    	 xsw.writeEndElement();
		    }
		    xsw.writeEndElement();
		    xsw.writeEndDocument();
		    xsw.flush();
		}catch (Exception e) {
			System.err.println("Unable to write the file: " + e.getMessage());
		}
		finally{
		    try{
		        if (xsw != null) {
		        	xsw.close();
		        }
		    }catch (Exception e) {
		            System.err.println("Unable to close the file: " + e.getMessage());
		    }
		}
	}
	
	private static class XMLHandler extends DefaultHandler {
		
		private String name, surname, phone, curentElementName, prevElementName;
		private BigDecimal price;
		//private Article article;
		private String article_name, condition, category;
		
		public ArrayList<OlxAd> res = new ArrayList();

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        	curentElementName = qName;
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
        	if(qName.equals("olx_ad")) {
        		if((name != null && !name.isEmpty()) &&
        	        (surname != null && !surname.isEmpty()) &&
        	        (phone != null && !phone.isEmpty()) &&
        	        (price != null) &&
        	        (article_name != null && !article_name.isEmpty()) &&
        	        (condition != null && !condition.isEmpty()) &&
        	        (category != null && !category.isEmpty())) {
        	        	res.add(new OlxAd(name, surname, phone, price, new Article(article_name, condition, category)));
        		}
        	}
        	prevElementName = curentElementName;
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
        	String information = new String(ch, start, length);

            information = information.replace("\n", "").trim();

            if (!information.isEmpty()) {
                switch (curentElementName){
            		case "name":
            			if(prevElementName != null && prevElementName.equals("price")) {
            				article_name = information;
            			}else{
            				name = information;
            			}
            			break;
            		case "surname":
            			surname = information;
            			break;
            		case "phone":
            			phone = information;
            			break;
            		case "price":
            			price = new BigDecimal(information);
            			break;
            		case "condition":
            			condition = information;
            			break;
            		case "category":
            			category = information;
            			break;
                }
            }
        }
    }

}